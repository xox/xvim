" Custom vimrc file.
"
" Maintainer:	Wenfeng CAI

source $VIMRUNTIME/gvimrc_example.vim

set mouse=a
:colo torte
set lines=36
set guifont=Monospace\ 16
" For Windows
" set guifont=Cascadia_Code:h16
